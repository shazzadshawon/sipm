<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID']))
{
	//$user= $_SESSION['SESS_MEMBER_ID'];
	//Check the database table for the logged in user information
	$check_user_details = mysql_query("select * from user where userId = '".mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"])."'");
	//Validate created session
	if(mysql_num_rows($check_user_details) < 1)
	{
		//echo 'Not in Member List';echo '<br>';
		session_unset();
		session_destroy();
		header("location: login.php");
	}
	elseif(mysql_num_rows($check_user_details) > 0)
	{
		//echo 'Member';echo '&nbsp;&nbsp;';
		$get_user_details = mysql_fetch_array($check_user_details);
		$role = strip_tags($get_user_details['role']);
		//echo $role;
		if($role!=3)
			{
				//echo 'But Not Authorised';echo '<br>';
				header("location: error.php");
				exit(); 
			}
			else
			{
				//echo 'Authorised';echo '<br>';
	
				//Get all the logged in user information from the database users table
				//$get_user_details = mysql_fetch_array($check_user_details);
				//echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status 
				require_once('auth.php');
				$fname = strip_tags($get_user_details['userFname']);
				$lname = strip_tags($get_user_details['userLname']);
				$mobileNo=strip_tags($get_user_details['mobileNo']);
				$userId = strip_tags($get_user_details['userId']);
				
				$role = strip_tags($get_user_details['role']);
				$barnchId=strip_tags($get_user_details['barnchId']);
 				$companyId=strip_tags($get_user_details['companyId']);
				
				
				
				
				$check_company_details = mysql_query("select * from company");	
				$get_company_details = mysql_fetch_array($check_company_details);
			
				$companyName = strip_tags($get_company_details['companyName']);
				$ownerName = strip_tags($get_company_details['ownerName']);
				$phoneNo=strip_tags($get_company_details['phoneNo']);
				$regNo=strip_tags($get_company_details['regNo']);
				$mobileNo = strip_tags($get_company_details['mobileNo']);
			
				$faxNo = strip_tags($get_company_details['faxNo']);
				$address = strip_tags($get_company_details['address']);
				$city=strip_tags($get_company_details['city']);
				$country = strip_tags($get_company_details['country']);
				$path = strip_tags($get_company_details['clogo']);
				$comEmail=strip_tags($get_company_details['comEmail']);
				$comWeb = strip_tags($get_company_details['comWeb']);
				
		//	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo
				
		include('func.php');		
  				
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Online Sales And Inventory Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	
	<!--for pdf -->
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<script type="text/javascript">
jQuery(function($){ //document ready
	$("#exportentry").submit(function(e){
		e.preventDefault();
		var data = $("#main-content").html();
		var filename = "test.pdf";
		if(data){
			$.ajax({
				type: "POST",
				url: "convert.php",
				data: {data: data, filename: filename, date: document.lastModified},
				dataType: "json", //try it without this if there's still a problem
				success: function(data){
					alert('success'); //to stop the success alert, remove or comment out this line
				}
			});
		}
		else {alert("No Report To Export");}
	});
});
</script>
	
	
	<!--///////////////////////////////////////////////-->
	
  </head>

  <body>
  
  
<form method="post" id="exportentry" action="">
<input type="submit" value="Create PDF">
</form>
  
  
  
  <section id="container" >
      <!--header start-->
      <header class="header white-bg">
            <?php include ("header.php");?>
        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <?php include("menu.php"); ?>
      </aside>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             Products Master Table
                          </header>
                          <div class="panel-body">
                                <div class="adv-table">
                                
                                <table  class="display table table-bordered table-striped" id="example">
                                      <thead>
                                      <tr>
                                          <th>Product Name</th>
                                          <th>Category</th>
                                          <th>Sub Category</th>
                                         <th>Brand</th>
										 <th>Unit Price</th>
										 <th>Min Order Level</th>
										 <th>Max Order Level</th>
										 <th>Return Level</th>
                                      </tr>
                                      </thead>
                                
                                 <?php


        // number of results to show per page

        $per_page = 100;

      // id 	title 	fname 	lname 	gender 	country 	district 	thana 	address 	mobile 	email 	username 	password 	introducer 	doj 	role 	dou 	status  

        // figure out the total pages in the database

        $result = mysql_query("SELECT * FROM product_t ORDER BY id DESC");

        
		
		
		$total_results = mysql_num_rows($result);
		
		
        $total_pages = ceil($total_results / $per_page);



        // check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)

        if (isset($_GET['page']) && is_numeric($_GET['page']))

        {

                $show_page = $_GET['page'];

                

                // make sure the $show_page value is valid

                if ($show_page > 0 && $show_page <= $total_pages)

                {

                        $start = ($show_page -1) * $per_page;

                        $end = $start + $per_page; 

                }

                else

                {

                        // error - show first set of results

                        $start = 0;

                        $end = $per_page; 

                }               

        }

        else

        {

                // if page isn't set, show first set of results

                $start = 0;

                $end = $per_page; 

        }

        

       

        // display data in table

        

		echo "</tbody>";
		
		
// memCatId 	memCategory 	memCatValue 	comValue 	mutual_com
        // loop through results of database query, displaying them in the table 

        for ($i = $start; $i < $end; $i++)

        {

                // make sure that PHP doesn't try to show results that don't exist

                if ($i == $total_results) { break; }

        

                // echo out the contents of each row into a table

				//jobCatid 	jobCategory 	status
				
                echo '<tr class="gradeA">';
				
				//trackId,username,investAmount

             //   echo '<td>' . mysql_result($result, $i, 'id') . '</td>';
				
				

               // echo '<td>' . mysql_result($result, $i, 'phone') . '</td>';

                echo '<td>' . mysql_result($result, $i, 'productName') . '</td>';
				
				$cat=mysql_result($result, $i, 'catId');
				$check_cat = mysql_query("select * from catgory where id = '$cat'");
				$row_cat=mysql_fetch_row($check_cat);	
				$catName=$row_cat[1];
				
				 echo '<td>' . $catName . '</td>';
				 
				$subcat=mysql_result($result, $i, 'subCatId');
				$check_subcat = mysql_query("select * from subcategory where id = '$subcat'");
				$row_subcat=mysql_fetch_row($check_subcat);	
				$subCatName=$row_subcat[1];
				
				 echo '<td>' . $subCatName . '</td>';
				 
				 $brnd=mysql_result($result, $i, 'brandId');
				$check_brnd = mysql_query("select * from brand_t where id = '$brnd'");
				$row_brnd=mysql_fetch_row($check_brnd);	
				$brandName=$row_brnd[1];
				
				 echo '<td>' . $brandName . '</td>';
				
				 echo '<td>' . mysql_result($result, $i, 'unitPrice') . '</td>';
				 
				 echo '<td>' . mysql_result($result, $i, 'minOrderQty') . '</td>';
				 
				 echo '<td>' . mysql_result($result, $i, 'maxOrderQty') . '</td>';
				 
				 echo '<td>' . mysql_result($result, $i, 'returnDays') . '</td>';
				
				
				
        		
				echo '<td>
                                     
                 <span class="btn btn-primary btn-xs"><li><a  href="productPolicy.php?id='.mysql_result($result, $i, 'id') . '">Edit</a></li></span>
				 <span class="btn btn-primary btn-xs"><li><a  href="editPolicy.php?id='.mysql_result($result, $i, 'id') . '">Policy</a></li></span>
                 <span class="btn btn-danger btn-xs"><li><a  href="productDelete.php?id='.mysql_result($result, $i, 'id') . '">Delete</a></li></span>                   
                 
			   </td>';
				

                echo "</tr>"; 

        
	}

        // close table>

        echo "</tbody>"; 

         echo "</table>";

        // pagination

		

        

?>
                                  
                                      
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
     <?php include("footer.php");?>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <!--<script src="js/jquery.js"></script>-->
    <script type="text/javascript" language="javascript" src="assets/advanced-datatable/media/js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
    <script src="js/respond.min.js" ></script>


  <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>

    <!--script for this page only-->

      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#example').dataTable( {
                  "aaSorting": [[ 4, "desc" ]]
              } );
          } );
      </script>
  </body>

</html>
<?php
	}
}

}
else
{
	header("location: login.php");
	exit(); 
}
	
?>