<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID']))
{
	//$user= $_SESSION['SESS_MEMBER_ID'];
	//Check the database table for the logged in user information
	$check_user_details = mysql_query("select * from user where userId = '".mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"])."'");
	//Validate created session
	if(mysql_num_rows($check_user_details) < 1)
	{
		//echo 'Not in Member List';echo '<br>';
		session_unset();
		session_destroy();
		header("location: login.php");
	}
	elseif(mysql_num_rows($check_user_details) > 0)
	{
		//echo 'Member';echo '&nbsp;&nbsp;';
		$get_user_details = mysql_fetch_array($check_user_details);
		$role = strip_tags($get_user_details['role']);
		//echo $role;
		if($role!=3)
			{
				//echo 'But Not Authorised';echo '<br>';
				header("location: error.php");
				exit(); 
			}
			else
			{
				//echo 'Authorised';echo '<br>';
	
				//Get all the logged in user information from the database users table
				//$get_user_details = mysql_fetch_array($check_user_details);
				//echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status 
				require_once('auth.php');
				$fname = strip_tags($get_user_details['userFname']);
				$lname = strip_tags($get_user_details['userLname']);
				$mobileNo=strip_tags($get_user_details['mobileNo']);
				$userId = strip_tags($get_user_details['userId']);
				
				$role = strip_tags($get_user_details['role']);
				$barnchId=strip_tags($get_user_details['barnchId']);
 				$companyId=strip_tags($get_user_details['companyId']);
				
				
				
				
				$check_company_details = mysql_query("select * from company");	
				$get_company_details = mysql_fetch_array($check_company_details);
			
				$companyName = strip_tags($get_company_details['companyName']);
				$ownerName = strip_tags($get_company_details['ownerName']);
				$phoneNo=strip_tags($get_company_details['phoneNo']);
				$regNo=strip_tags($get_company_details['regNo']);
				$mobileNo = strip_tags($get_company_details['mobileNo']);
			
				$faxNo = strip_tags($get_company_details['faxNo']);
				$address = strip_tags($get_company_details['address']);
				$city=strip_tags($get_company_details['city']);
				$country = strip_tags($get_company_details['country']);
				$path = strip_tags($get_company_details['clogo']);
				$comEmail=strip_tags($get_company_details['comEmail']);
				$comWeb = strip_tags($get_company_details['comWeb']);
				
		//	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo
				
				
  				
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Taibur Rahman">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Online Sales And Inventory Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    
      <script src="js/jquery.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script>
	$( document ).ready(function() {
		$("#txtphoneNo").hide();
		$("#txtmobileNo").hide();
		$("#txtfaxNo").hide();
		$("#txtemailId").hide();
		$("#txtwebAdd").hide();
		$("#txtaddressB").hide();
		$("#frmUpdate").hide();
		
		
		
		
		
		$("#addDetails").click(function(){
		$("#txtphoneNo").show();
		$("#txtmobileNo").show();
		$("#txtfaxNo").show();
		$("#txtemailId").show();
		$("#txtwebAdd").show();
		$("#txtaddressB").show();
		$("#frmUpdate").show();
	});
	});
	

	</script>
    
    
    
    
    
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header white-bg">
            <?php include ("header.php");?>
        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <?php include("menu.php"); ?>
      </aside>
      <!--sidebar end-->
      <!--main content start //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo -->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <?php 
			  	$branchId = $_GET['bid'];
				$check_branch = mysql_query("select * from location where locationId = '$branchId'");
				$row_branch=mysql_fetch_row($check_branch);	
				$branchName=$row_branch[1];
				
				$check_branch_details = mysql_query("select * from locationdetails where locationId = '$branchId'");
				$row_branch_details=mysql_fetch_row($check_branch_details);	
				$no_row=mysql_num_rows($check_branch_details);
				
				$locationAddress=$row_branch_details[3];
				$locationPhone=$row_branch_details[4];
				$locationMobile=$row_branch_details[5];
				$locationFax=$row_branch_details[6];
				$locationEmail=$row_branch_details[7];
				$locationWeb=$row_branch_details[8];
				
				
	// id 	locationId 	locationName 	locationAddress 	locationPhone 	locationMobile 	locationFax 	locationEmail 	locationWeb 	locationStatus 
	

				
				
				
			  ?>
              <div class="row">
                  <div class="col-lg-6">
                      <section class="panel">
                          <header class="panel-heading">
                             <?php echo $branchName;?> Branch <button id="addDetails" class="btn btn-info" type="button" style="float:right">Update Details</button>
                          </header>
                          
                          <?php 
						  
							  	// id 	locationId 	locationName 	locationAddress 	locationPhone 	locationMobile 	locationFax 	locationEmail 	locationWeb 	locationStatus   

							  echo '<div class="panel-body">
                              <form class="form-horizontal" action="branchdetailsInc.php" method="POST" >
							   <input type="hidden" id="branch" name="branch" value="'.$branchId.'" class="form-control">
                               <input type="hidden" id="branchName" name="branchName" value="'.$branchName.'" class="form-control">
                                  <div class="form-group">
                                      <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Phone No</label>
                                      <div class="col-lg-10">
                                          <input type="text" placeholder="Phone No" id="txtphoneNo" name="locationPhone" value="'.$locationPhone.'" class="form-control">
                                          <p class="help-block">'.$locationPhone.'</p>
                                      </div>
                                  </div>
                                 <div class="form-group">
                                      <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Mobile No</label>
                                      <div class="col-lg-10">
                                          <input type="text" placeholder="Mobile No" id="txtmobileNo" name="locationMobile" value="'.$locationMobile.'" class="form-control">
                                        <p class="help-block">'.$locationMobile.'</p>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Fax No</label>
                                      <div class="col-lg-10">
                                          <input type="text" placeholder="Fax No" id="txtfaxNo" name="locationFax" value="'.$locationFax.'" class="form-control">
                                          <p class="help-block">'.$locationFax.'</p>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Email Address</label>
                                      <div class="col-lg-10">
                                          <input type="email" placeholder="Email Id" id="txtemailId" name="locationEmail" value="'.$locationEmail.'" class="form-control">
                                          <p class="help-block">'.$locationEmail.'</p>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Web Address</label>
                                      <div class="col-lg-10">
                                          <input type="text" placeholder="Web Address" id="txtwebAdd" name="locationWeb" value="'.$locationWeb.'" class="form-control">
                                         <p class="help-block">'.$locationWeb.'</p>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Branch Address</label>
                                      <div class="col-lg-10">
                                          <textarea id="txtaddressB" name="locationAddress" rows="4" cols="50" class="form-control">'.$locationAddress.'</textarea>
                                         <p class="help-block">'.$locationAddress.'</p>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-lg-offset-2 col-lg-10">
                                          <button id="frmUpdate" class="btn btn-danger" type="submit">Sign in</button>
                                      </div>
                                  </div>
                              </form>
                          </div>';
						  
						  
						  ?>
                          
                         
                      </section>
                  </div>
               
                  
              </div>
              
           

              
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
     <?php include("footer.php");?>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="js/owl.carousel.js" ></script>
    <script src="js/jquery.customSelect.min.js" ></script>
    <script src="js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
    <script src="js/count.js"></script>
   
  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>

</html>
<?php
	}
}

}
else
{
	header("location: login.php");
	exit(); 
}
	
?>