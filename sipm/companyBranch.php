<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID']))
{
	//$user= $_SESSION['SESS_MEMBER_ID'];
	//Check the database table for the logged in user information
	$check_user_details = mysql_query("select * from user where userId = '".mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"])."'");
	//Validate created session
	if(mysql_num_rows($check_user_details) < 1)
	{
		//echo 'Not in Member List';echo '<br>';
		session_unset();
		session_destroy();
		header("location: login.php");
	}
	elseif(mysql_num_rows($check_user_details) > 0)
	{
		//echo 'Member';echo '&nbsp;&nbsp;';
		$get_user_details = mysql_fetch_array($check_user_details);
		$role = strip_tags($get_user_details['role']);
		//echo $role;
		if($role!=3)
			{
				//echo 'But Not Authorised';echo '<br>';
				header("location: error.php");
				exit(); 
			}
			else
			{
				//echo 'Authorised';echo '<br>';
	
				//Get all the logged in user information from the database users table
				//$get_user_details = mysql_fetch_array($check_user_details);
				//echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status 
				require_once('auth.php');
				$fname = strip_tags($get_user_details['userFname']);
				$lname = strip_tags($get_user_details['userLname']);
				$mobileNo=strip_tags($get_user_details['mobileNo']);
				$userId = strip_tags($get_user_details['userId']);
				
				$role = strip_tags($get_user_details['role']);
				$barnchId=strip_tags($get_user_details['barnchId']);
 				$companyId=strip_tags($get_user_details['companyId']);
				
				
				
				
				$check_company_details = mysql_query("select * from company");	
				$get_company_details = mysql_fetch_array($check_company_details);
			
				$companyName = strip_tags($get_company_details['companyName']);
				$ownerName = strip_tags($get_company_details['ownerName']);
				$phoneNo=strip_tags($get_company_details['phoneNo']);
				$regNo=strip_tags($get_company_details['regNo']);
				$mobileNo = strip_tags($get_company_details['mobileNo']);
			
				$faxNo = strip_tags($get_company_details['faxNo']);
				$address = strip_tags($get_company_details['address']);
				$city=strip_tags($get_company_details['city']);
				$country = strip_tags($get_company_details['country']);
				$path = strip_tags($get_company_details['clogo']);
				$comEmail=strip_tags($get_company_details['comEmail']);
				$comWeb = strip_tags($get_company_details['comWeb']);
				
		//	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo
				
				
  				
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Online Sales And Inventory Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

	<script src="js/jquery-1.8.3.min.js"></script>
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	<script type="text/javascript" >
    $(function() {
    $("#submit").click(function() {
	if($('#b').val() == ''){
      alert('Branch name is required!');
   }else{
   var branchName = $("#b").val();

    var dataString = 'bname='+ branchName;


   $.ajax({
         type: "POST",
         url: "branchValidate.php",
         data: dataString,
         success: function(data) 
       {
         bnm = data;
 
       if (bnm == 1)
       {
       alert('Branch name already exist!'); 
      }else{
       $.ajax({
	   type: "POST",
	   url: "addBranch.php",
	   data: dataString,
	   success: function(data){
	   alert(data+' branch added successfully!');
	   window.location="companyBranch.php";
	  }
	});
   }
     
            
 }
});
   }
	
    

});
});
</script>
	
	
	
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header white-bg">
            <?php include ("header.php");?>
        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <?php include("menu.php"); ?>
      </aside>
      <!--sidebar end-->
      <!--main content start //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo -->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <section class="panel">
                          <header class="panel-heading tab-bg-dark-navy-blue tab-right ">
                              <ul class="nav nav-tabs pull-right">
                                  <li class="active">
                                      <a href="#home-3" data-toggle="tab">
                                          <i class="fa fa-home">&nbsp;Branch List</i>
                                      </a>
                                  </li>
                                 
                                  <li class="">
                                      <a href="#contact-3" data-toggle="tab">
                                          <i class="fa fa-plus-square"></i>
                                          Add Branch
                                      </a>
                                  </li>
                              </ul>
                              <span class="hidden-sm wht-color">Company Branch List</span>
                          </header>
                          <div class="panel-body">
                              <div class="tab-content">
                                  <div class="tab-pane active" id="home-3">
                                  
                            <?php


        // number of results to show per page

        $per_page = 100;

      // id 	title 	fname 	lname 	gender 	country 	district 	thana 	address 	mobile 	email 	username 	password 	introducer 	doj 	role 	dou 	status  

        // figure out the total pages in the database

        $result = mysql_query("SELECT * FROM location ORDER BY locationId DESC");

        
		
		
		$total_results = mysql_num_rows($result);
		
		if($total_results<=0)
		{
			echo '<p style="text-align:center; font-weight:bold">There is no Information available Now</p>';
		}
		else
		{
		echo '<table class="table table-bordered table-striped table-condensed cf">
                                      <thead class="cf">
                                      <tr>
                                          <th>Branch Id</th>
                                          <th>Branch Name</th>
                                          <th>Status</th>
                                          <th>Action</th>
                                          
                                         
                                      </tr>
                                      </thead>';
        $total_pages = ceil($total_results / $per_page);



        // check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)

        if (isset($_GET['page']) && is_numeric($_GET['page']))

        {

                $show_page = $_GET['page'];

                

                // make sure the $show_page value is valid

                if ($show_page > 0 && $show_page <= $total_pages)

                {

                        $start = ($show_page -1) * $per_page;

                        $end = $start + $per_page; 

                }

                else

                {

                        // error - show first set of results

                        $start = 0;

                        $end = $per_page; 

                }               

        }

        else

        {

                // if page isn't set, show first set of results

                $start = 0;

                $end = $per_page; 

        }

        

       

        // display data in table

        

		echo "</tbody>";
		
		
// memCatId 	memCategory 	memCatValue 	comValue 	mutual_com
        // loop through results of database query, displaying them in the table 

        for ($i = $start; $i < $end; $i++)

        {

                // make sure that PHP doesn't try to show results that don't exist

                if ($i == $total_results) { break; }

        

                // echo out the contents of each row into a table

				//jobCatid 	jobCategory 	status
				
                echo "<tr>";
				
				//trackId,username,investAmount

                echo '<td>' . mysql_result($result, $i, 'locationId') . '</td>';
				
				

               // echo '<td>' . mysql_result($result, $i, 'phone') . '</td>';

                echo '<td>' . mysql_result($result, $i, 'locationName') . '</td>';
				
				echo '<td>' . mysql_result($result, $i, 'status') . '</td>';
				
				
				
        		
			
				echo '<td>
			   <span class="label label-info label-mini"><a style="color:#fff" href="EditcompanyBranch.php?bid='.mysql_result($result, $i, 'locationId') . '">Edit</a></span>
			   <span class="label label-info label-mini"><a style="color:#fff" href="DeletecompanyBranch.php?bid='.mysql_result($result, $i, 'locationId') . '">Delete</a></span>
               <span class="label label-info label-mini"><a style="color:#fff" href="branchDetails.php?bid='.mysql_result($result, $i, 'locationId') . '">Details</a></span>
               <span class="label label-info label-mini"><a style="color:#fff" href="branchEmployer.php?bid='.mysql_result($result, $i, 'locationId') . '">Branch Employer</a></span>
                                      
                                  </td>';

                echo "</tr>"; 

        }
	}

        // close table>

        echo "</tbody>"; 

         echo "</table>";

        // pagination

		

        

?>
                            
                            
                            
                            
                        
                       <?php 
echo '<div id="paginate" class="dataTables_paginate paging_full_numbers" style="height:20px;"><span style="color:#000;">View Page : &nbsp;</span>';
echo '<span>';
        for ($i = 1; $i <= $total_pages; $i++)

        {
				
                echo "<a id='page_a_link' class='paginate_button' href='companyBranch.php?page=$i'>$i</a>";

        }
echo '</span>';
        echo "</div>";
//?> 
                                  
                                  </div>
                                  
                                  <div class="tab-pane" id="contact-3">
                                  		<section class="panel">
                          <header class="panel-heading">
                              Add Branch
                          </header>
                          <div class="panel-body">                                                           
                                  <div class="form-group">
                                      <label for="exampleInputEmail2" class="sr-only">Branch Name</label>
                                      <input type="text" placeholder="Enter branch name" id="b" name="b" class="form-control">
                                  </div>                                 
                                  
                                  <button id="submit" name="submit" class="btn btn-success">Add Branch</button>						 
                             
                          </div>
                      </section>
                                  		
                                  </div>
                              </div>
                          </div>
                      </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
     <?php include("footer.php");?>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
   
    <script src="js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="js/owl.carousel.js" ></script>
    <script src="js/jquery.customSelect.min.js" ></script>
    <script src="js/respond.min.js" ></script>
	 
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
    <script src="js/count.js"></script>
	
	
	
  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>

</html>
<?php
	}
}

}
else
{
	header("location: login.php");
	exit(); 
}
	
?>