/*---LEFT BAR ACCORDION----*/
$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});

var Script = function () {

//    sidebar dropdown menu auto scrolling

    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if(diff>0)
            $("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            $("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

//    sidebar toggle

    // $(function() {
    //     function responsiveView() {
    //         var wSize = $(window).width();
    //         if (wSize <= 768) {
    //             $('#container').addClass('sidebar-close');
    //             $('#sidebar > ul').hide();
    //         }
    //
    //         if (wSize > 768) {
    //             $('#container').removeClass('sidebar-close');
    //             $('#sidebar > ul').show();
    //         }
    //     }
    //     $(window).on('load', responsiveView);
    //     $(window).on('resize', responsiveView);
    // });

    // $('.fa-bars').click(function () {
    //     if ($('#sidebar > ul').is(":visible") === true) {
    //         $('#main-content').css({
    //             'margin-left': '0px'
    //         });
    //         $('#sidebar').css({
    //             'margin-left': '-210px'
    //         });
    //         $('#sidebar > ul').hide();
    //         $("#container").addClass("sidebar-closed");
    //     } else {
    //         $('#main-content').css({
    //             'margin-left': '210px'
    //         });
    //         $('#sidebar > ul').show();
    //         $('#sidebar').css({
    //             'margin-left': '0'
    //         });
    //         $("#container").removeClass("sidebar-closed");
    //     }
    // });

// custom scrollbar
    $("#sidebar").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});

    $("html").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled:false,  cursorborder: '', zindex: '1000'});

// widget tools

    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });


//    tool tips

    $('.tooltips').tooltip();

//    popovers

    $('.popovers').popover();



// custom bar chart

    if ($(".custom-bar-chart")) {
        $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }

}();

$(window).load(function(){

    $('.ovmenref').click(function(){
       var reff = $(this).attr('clickTo');
       window.location.href = reff;
    });

    var DELAY = 700,
        clicks = 0,
        timer = null;

    $('.overlay_container').live("click", function(e){
        clicks++;
        if(clicks === 1) {
            timer = setTimeout(function() {
                // alert('Single Click');
                clicks = 0;
            }, DELAY);
        } else {
            clearTimeout(timer);
            // alert('Double Click');
            clicks = 0;
        }
    }).live("dblclick", function(e){
        e.preventDefault();
    });

    $('.flipCard').flip({
        axis: 'y',
        speed: 150,
        trigger: 'hover'
    }).on('flip:done', function(){
        var h = $(this).find('.back').find('.menu_inner_item').height();
        if(h > 250){
            $(this).find('.overlay_item').niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});
        }
    });

    $(document).keyup(function(e){
        if(e.keyCode == 27){
            if ($('.full-menu').hasClass('full-menu--open')){
                console.log('Going to close the Menu');
                closeNav();
            }
        }
    });
});

function procesBarCode(node, target, drawTarget){
    target.scannerDetection({
        timeBeforeScanTest: 200,
        startChar: [120],
        endChar: [13],
        avgTimeByChar: 40,
        onComplete: function(barcode, qty){
            node.val(barcode);
            createBarCode(drawTarget, barcode)
        }
    });
}

function createBarCode(node, input){
    JsBarcode(node, input, {
        lineColor: "#5b6e84",
        background: "transparent",
        textPosition: "top"
    });
}