<?php
echo '<div id="sidebar"  class="nav-collapse ">
              
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a class="active" href="index.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-laptop"></i>
                          <span>Company Setting</span>
                      </a>
                      <ul class="sub">                                                   
                          <li><a  href="employer.php">Employer List</a></li>
                          <li><a  href="#" target="_blank">Set Invoice</a></li>
                      </ul>
                  </li>
 					 <li class="sub-menu"><a  href="#">Customer Details</a>
                          		<ul class="sub">
                                  <li><a  href="customerAdd.php">Add Customer</a></li>
                                  <li><a  href="customerMaster.php">Customer List</a></li>
                                  <li><a  href="customerLedger.php">Customer Ladger</a></li>
                               </ul>
                          </li>
						   <li class="sub-menu"><a  href="#">Supplier Details</a>
                          		<ul class="sub">
                                  <li><a  href="supplierAdd.php">Add Supplier</a></li>
                                  <li><a  href="supplierMaster.php">Supplier List</a></li>
                                  <li><a  href="supplierLedger.php">Supplier Ladger</a></li>
                               </ul>
                          </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Products Details</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="productMaster.php">Product Master</a></li>
                          <li><a  href="#">Product Policy</a></li>
                         
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Purchase</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="#">Purchase Product</a></li>
                          <li><a  href="#">Purchase Product History</a></li>
                          <li><a  href="#">Return Product</a></li>
                          <li><a  href="#">Return Product List</a></li>
                         
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Sales</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="invoice.html">New Invoice</a></li>
                          <li><a  href="#">Invoice List</a></li>
                          <li><a  href="#">Return Product</a></li>
                          <li><a  href="#">Credit Payment</a></li>
                         
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Report</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="#">Custom Report</a></li>
                          <li><a  href="#">Purchase Report</a></li>
                          <li><a  href="#">Inventory Report</a></li>
                          <li><a  href="#">Sales Report</a></li>
                           <li><a  href="#">Accounts Payable</a></li>
                          <li><a  href="#">Account Receiveable</a></li>
                          
                      </ul>
                  </li>
                 
                  
                  <li>
                      <a  href="login.html">
                          <i class="fa fa-user"></i>
                          <span>Login Page</span>
                      </a>
                  </li>

                 

              </ul>
             
          </div>';
?>