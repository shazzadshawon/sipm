 <?php

require ('html2fpdf.php');

if (isset ($_POST['data'])) {
    $urlcontents = $_POST ['data'];
    $filename = $_POST ['filename'];
    $date = isset ($_POST ['date'])? $_POST ['date']: '5/25/92';
    convert ($urlcontents, $filename, $date);
}

function convert ($contents, $name, $currdate) {
    $pdf = new HTML2FPDF ();
    $pdf-> AddPage ();
    $pdf-> SetFont ('Arial', 'B', 14);
    $pdf-> Cell (120, 10, "Report Entry - $currdate");
    $pdf-> SetFont ('Arial','', 12);
    $pdf-> Sety (20);
    $pdf-> WriteHTML ($contents);
    $pdf-> Output ($name, 'F');
}
?> 