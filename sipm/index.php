<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);

            ?>

            <!DOCTYPE html>
            <html lang="en">

            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="">
                <meta name="author" content="Mosaddek">
                <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
                <link rel="shortcut icon" href="img/favicon.html">

                <title>Online Sales And Inventory Management System</title>

                <!-- Full Screen Overlay Menu-->
                <link href="css/overlay_menu.css" rel="stylesheet" />
                <!-- Full Screen Overlay Menu-->

                <!-- Bootstrap core CSS -->
                <link href="css/bootstrap.min.css" rel="stylesheet">
                <link href="css/bootstrap-reset.css" rel="stylesheet">
                <!--external css-->
                <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
                <link href="css/font-awsome_animation.min.css" rel="stylesheet" />
                <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"
                      media="screen"/>
                <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
                <!-- Custom styles for this template -->
                <link href="css/style.css" rel="stylesheet">
                <link href="css/style-responsive.css" rel="stylesheet"/>

                <script src="js/jquery-1.8.3.min.js"></script>

                <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
                <!--[if lt IE 9]>
                <script src="js/html5shiv.js"></script>
                <script src="js/respond.min.js"></script>
                <![endif]-->

                <script>
                    function showEmployer(str) {
                        $('#defaultTbl').hide();
                        if (str == "") {
                            document.getElementById("txtEmployer").innerHTML = "";
                            return;
                        }
                        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                            xmlhttp = new XMLHttpRequest();
                        }
                        else {// code for IE6, IE5
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function () {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                document.getElementById("txtEmployer").innerHTML = xmlhttp.responseText;
                            }
                        }
                        xmlhttp.open("GET", "getEmployer.php?bid=" + str, true);
                        xmlhttp.send();
                    }

                    function openNav(){
                        $('.full-menu').toggleClass('full-menu--open');
                        $('#nav-icon2').addClass('open');
                    }

                    function closeNav(){
                        $('.full-menu').removeClass('full-menu--open');
                        $('#nav-icon2').removeClass('open');
                    }
                </script>

            </head>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->
                <!--sidebar start-->
<!--                <aside>-->
<!--                    --><?php //include("menu.php"); ?>
<!--                </aside>-->
                <div>
                    <?php require("overlayMenu.php"); ?>
                </div>

                <!--sidebar end-->
                <!--main content start-->
                <section id="main-content">
                    <section class="wrapper">
                        <!--state overview start-->
                        <div class="row state-overview">
                            <div class="col-lg-3 col-sm-6">
                                <section class="panel">
                                    <div class="symbol terques">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <div class="value">
                                        <h1 class="count">
                                            0
                                        </h1>
                                        <p>Total Products</p>
                                    </div>
                                </section>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <section class="panel">
                                    <div class="symbol red">
                                        <i class="fa fa-tags"></i>
                                    </div>
                                    <div class="value">
                                        <h1 class=" count2">
                                            0
                                        </h1>
                                        <p>Sales Revenue</p>
                                    </div>
                                </section>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <section class="panel">
                                    <div class="symbol yellow">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="value">
                                        <h1 class=" count3">
                                            0
                                        </h1>
                                        <p>Total Accounts Payable</p>
                                    </div>
                                </section>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <section class="panel">
                                    <div class="symbol blue">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="value">
                                        <h1 class=" count4">
                                            0
                                        </h1>
                                        <p>Total Accounts Receiveable</p>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!--state overview end-->


                        <div class="row">
                            <div class="col-lg-4">
                                <!--user info table start-->
                                <section class="panel">
                                    <div class="panel-body">

                                        <div class="task-thumb-details" style="margin-top:0">
                                            <h1><a href="#">Branch List</a></h1>

                                        </div>
                                    </div>
                                    <?php


                                    // number of results to show per page

                                    $per_page = 100;

                                    // id 	title 	fname 	lname 	gender 	country 	district 	thana 	address 	mobile 	email 	username 	password 	introducer 	doj 	role 	dou 	status

                                    // figure out the total pages in the database

                                    $result = mysql_query("SELECT * FROM location ORDER BY locationId DESC");


                                    $total_results = mysql_num_rows($result);

                                    if ($total_results <= 0) {
                                        echo '<p style="text-align:center; font-weight:bold">There is no information available now</p>';
                                    } else {
                                        echo '<table class="table table-bordered table-striped table-condensed cf">
                                      <thead class="cf">
                                      <tr>
                                            <th>Branch Name</th>
                                            <th>Employee number</th>
                                          
                                         
                                      </tr>
                                      </thead>';
                                        $total_pages = ceil($total_results / $per_page);


                                        // check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)

                                        if (isset($_GET['page']) && is_numeric($_GET['page'])) {

                                            $show_page = $_GET['page'];


                                            // make sure the $show_page value is valid

                                            if ($show_page > 0 && $show_page <= $total_pages) {

                                                $start = ($show_page - 1) * $per_page;

                                                $end = $start + $per_page;

                                            } else {

                                                // error - show first set of results

                                                $start = 0;

                                                $end = $per_page;

                                            }

                                        } else {

                                            // if page isn't set, show first set of results

                                            $start = 0;

                                            $end = $per_page;

                                        }


                                        // display data in table


                                        echo "</tbody>";


// memCatId 	memCategory 	memCatValue 	comValue 	mutual_com
                                        // loop through results of database query, displaying them in the table

                                        for ($i = $start; $i < $end; $i++) {

                                            // make sure that PHP doesn't try to show results that don't exist

                                            if ($i == $total_results) {
                                                break;
                                            }


                                            // echo out the contents of each row into a table

                                            //jobCatid 	jobCategory 	status

                                            echo "<tr>";

                                            echo '<td>' . mysql_result($result, $i, 'locationName') . '</td>';

                                            $barnchId = mysql_result($result, $i, 'locationId');

                                            $getUser = mysql_query("select COUNT( DISTINCT id ) AS uc from user where barnchId = '$barnchId'");
                                            $row_result = mysql_fetch_row($getUser);
                                            $u = $row_result[0];
                                            echo '<td>' . $u . '</td>';


                                            echo "</tr>";

                                        }
                                    }

                                    // close table>

                                    echo "</tbody>";

                                    echo "</table>";

                                    // pagination


                                    ?>
                                </section>
                                <!--user info table end-->
                            </div>
                            <div class="col-lg-8">
                                <!--work progress start-->
                                <section class="panel">
                                    <div class="panel-body progress-panel">
                                        <div class="task-progress">
                                            <h1>Employer List</h1>

                                        </div>
                                        <div class="task-option">
                                            <?php
                                            $query = "SELECT * FROM location";
                                            $result = mysql_query($query);


                                            echo ' <select id="branchId" name="branchId" class="form-control" onchange="showEmployer(this.value)">';
                                            echo ' <option value="" selected="selected" disabled="disabled">Choose branch</option>   ';

                                            while ($row = mysql_fetch_assoc($result)) {
                                                echo '  <option value="' . $row['locationId'] . '">&nbsp;&nbsp;' . htmlspecialchars($row['locationName']) . '</option>';
                                            }
                                            echo "</select>"; ?>
                                        </div>
                                    </div>
                                    <div id="txtEmployer"><b></b></div>

                                    <div id="defaultTbl" class="adv-table">
                                        <table class="display table table-bordered table-striped" id="example">
                                            <thead>
                                            <tr>
                                                <th>Employer Name</th>
                                                <th>Branch</th>
                                            </tr>
                                            </thead>
                                            <?php


                                            // number of results to show per page

                                            $per_page = 50;

                                            // id 	title 	fname 	lname 	gender 	country 	district 	thana 	address 	mobile 	email 	username 	password 	introducer 	doj 	role 	dou 	status

                                            // figure out the total pages in the database


                                            $result = mysql_query("SELECT * FROM user WHERE role !='3' ORDER BY id DESC");


                                            $total_results = mysql_num_rows($result);

                                            $total_pages = ceil($total_results / $per_page);


                                            // check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)

                                            if (isset($_GET['page']) && is_numeric($_GET['page'])) {

                                                $show_page = $_GET['page'];


                                                // make sure the $show_page value is valid

                                                if ($show_page > 0 && $show_page <= $total_pages) {

                                                    $start = ($show_page - 1) * $per_page;

                                                    $end = $start + $per_page;

                                                } else {

                                                    // error - show first set of results

                                                    $start = 0;

                                                    $end = $per_page;

                                                }

                                            } else {

                                                // if page isn't set, show first set of results

                                                $start = 0;

                                                $end = $per_page;

                                            }


                                            // display data in table


                                            echo "</tbody>";


                                            // memCatId 	memCategory 	memCatValue 	comValue 	mutual_com
                                            // loop through results of database query, displaying them in the table

                                            for ($i = $start; $i < $end; $i++) {

                                                // make sure that PHP doesn't try to show results that don't exist

                                                if ($i == $total_results) {
                                                    break;
                                                }


                                                // echo out the contents of each row into a table

                                                //jobCatid 	jobCategory 	status

                                                echo "<tr>";

                                                //trackId,username,investAmount

                                                echo '<td>' . mysql_result($result, $i, 'userFname') . ' ' . mysql_result($result, $i, 'userLname') . '</td>';


                                                $branch = mysql_result($result, $i, 'barnchId');
                                                $check_branch = mysql_query("select * from location where locationId = '$branch'");
                                                $row_branch = mysql_fetch_row($check_branch);
                                                $branchId = $row_branch[1];

                                                echo '<td>' . $branchId . '</td>';


                                                echo "</tr>";

                                            }

                                            // close table>

                                            echo "</tbody>";

                                            echo "</table>";

                                            ?>
                                </section>
                                <!--work progress end-->
                            </div>
                        </div>

                        <!--    TEST BARCODE READER-->
                        <input id="barcodeScanner" type="text" class="form-control" name="barcode" placeholder="barcode Test" />
                        <svg id="barcodeOutput"></svg>
                        <!--    TEST BARCODE READER-->

                    </section>
                </section>
                <!--main content end-->
                <!--footer start-->
                <?php include("footer.php"); ?>
                <!--footer end-->
            </section>

            <!-- js placed at the end of the document so the pages load faster -->
            <script src="js/jquery.js"></script>
            <script src="js/jquery-1.8.3.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
            <script src="js/flip.js"></script>
            <script src="js/jquery.scrollTo.min.js"></script>
            <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
            <script src="js/jquery.sparkline.js" type="text/javascript"></script>
            <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
            <script src="js/owl.carousel.js"></script>
            <script src="js/jquery.customSelect.min.js"></script>
            <script src="js/respond.min.js"></script>

            <script type="text/javascript" src="js/jquery.scannerdetection.js"></script>
            <script type="text/javascript" src="js/jsBarcode.min.js"></script>
            <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

            <!--common script for all pages-->
            <script src="js/common-scripts.js"></script>

            <!--script for this page-->
            <script src="js/sparkline-chart.js"></script>
            <script src="js/easy-pie-chart.js"></script>
            <script src="js/count.js"></script>

            <script>

                //owl carousel

                $(document).ready(function () {
                    $("#owl-demo").owlCarousel({
                        navigation: true,
                        slideSpeed: 300,
                        paginationSpeed: 400,
                        singleItem: true,
                        autoPlay: true

                    });
                });

                //custom select box

                $(function () {
                    $('select.styled').customSelect();
                });

                var node = $('#barcodeScanner');
                var target = $(document);
                procesBarCode(node, target, '#barcodeOutput');
            </script>

            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>