<?php?>

<div id="sidebar"  class="nav-collapse ">
              
  <ul class="sidebar-menu" id="nav-accordion">
      <li>
          <a class="active" href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
          </a>
      </li>

      <li class="sub-menu">
          <a href="javascript:;" >
              <i class="fa fa-laptop"></i>
              <span>Company Setting</span>
          </a>
          <ul class="sub">
              <li><a  href="companyProfile.php">Company Details</a></li>
              <li><a  href="companyBranch.php">Branch Details</a>
                
              </li>
              <li><a  href="employer.php">Employer List</a></li>                      
              <li><a  href="setInvoiceFormat.php">Set Invoice</a></li>
              <li><a  href="settingVat.php">Setting</a></li>
          </ul>
      </li>
      <li class="sub-menu"><a  href="#">Customer Details</a>
        <ul class="sub">
            <li><a  href="customerAdd.php">Add Customer</a></li>
            <li><a  href="customerMaster.php">Customer List</a></li>
            <li><a  href="customerLedger.php">Customer Ladger</a></li>
         </ul>
      </li>
      <li class="sub-menu"><a  href="#">Supplier Details</a>
                  <ul class="sub">
                      <li><a href="supplierAdd.php">Add Supplier</a></li>
                      <li><a href="supplierMaster.php">Supplier List</a></li>
                      <li><a href="supplyLedger.php">Supplier Ladger</a></li>
      <li><a href="supplierPayment.php">Supplier Payment</a></li>
                   </ul>
              </li>
      <li class="sub-menu">
          <a href="javascript:;" >
              <i class="fa fa-book"></i>
              <span>Products Details</span>
          </a>
          <ul class="sub">
              <li><a  href="category.php">Category</a></li>
  <li><a  href="subCategory.php">Sub Category</a></li>
 
  <li><a  href="brand.php">Brand</a></li>
  <li><a  href="productAdd.php">Add Product</a></li>
              <li><a  href="productMaster.php">Product Master</a></li>
              <li><a  href="allproductPolicy.php">Product Policy</a></li>
             
          </ul>
      </li>

      <li class="sub-menu">
          <a href="javascript:;" >
              <i class="fa fa-cogs"></i>
              <span>Purchase</span>
          </a>
          <ul class="sub">
              <li><a  href="purchaseOrder.php">Purchase Product</a></li>
              <li><a  href="pph.php">Purchase Product History</a></li>
              <li><a  href="productReturn.php">Return Product</a></li>
              <li><a  href="returnProductList.php">Return Product List</a></li>
             
          </ul>
      </li>
      <li class="sub-menu">
          <a href="javascript:;" >
              <i class="fa fa-tasks"></i>
              <span>Sales</span>
          </a>
          <ul class="sub">
              <li><a  href="invoice.php">New Invoice</a></li>
              <li><a  href="#">Invoice List</a></li>
              <li><a  href="#">Return Product</a></li>
              <li><a  href="#">Credit Payment</a></li>
             
          </ul>
      </li>
      
      <li class="sub-menu">
          <a href="javascript:;" >
              <i class="fa fa-th"></i>
              <span>Report</span>
          </a>
          <ul class="sub">
              <!--<li><a  href="#">Custom Report</a></li>-->
              <li><a  href="purchaseLedger.php">Purchase Report</a></li>
              <li><a  href="stockDetails.php">Inventory Report</a></li>
              <li><a  href="#">Sales Report</a></li>
              <li><a  href="#">Accounts Payable</a></li>
              <li><a  href="#">Account Receiveable</a></li>
              
          </ul>
      </li>
     
      
     <!-- 
     <li>
          <a  href="login.php">
              <i class="fa fa-user"></i>
              <span>Login Page</span>
          </a>
      </li>
      -->

     

  </ul>
 
</div>