<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID']))
{
	//$user= $_SESSION['SESS_MEMBER_ID'];
	//Check the database table for the logged in user information
	$check_user_details = mysql_query("select * from user where userId = '".mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"])."'");
	//Validate created session
	if(mysql_num_rows($check_user_details) < 1)
	{
		//echo 'Not in Member List';echo '<br>';
		session_unset();
		session_destroy();
		header("location: login.php");
	}
	elseif(mysql_num_rows($check_user_details) > 0)
	{
		//echo 'Member';echo '&nbsp;&nbsp;';
		$get_user_details = mysql_fetch_array($check_user_details);
		$role = strip_tags($get_user_details['role']);
		//echo $role;
		if($role!=3 && $role!=5)
			{
				//echo 'But Not Authorised';echo '<br>';
				header("location: error.php");
				exit(); 
			}
			else
			{
				//echo 'Authorised';echo '<br>';
	
				//Get all the logged in user information from the database users table
				//$get_user_details = mysql_fetch_array($check_user_details);
				//echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status 
				require_once('auth.php');
				$fname = strip_tags($get_user_details['userFname']);
				$lname = strip_tags($get_user_details['userLname']);
				$mobileNo=strip_tags($get_user_details['mobileNo']);
				$userId = strip_tags($get_user_details['userId']);
				
				$role = strip_tags($get_user_details['role']);
				$barnchId=strip_tags($get_user_details['barnchId']);
 				$companyId=strip_tags($get_user_details['companyId']);
				
				
				
				
			$check_company_details = mysql_query("select * from company");	
			$get_company_details = mysql_fetch_array($check_company_details);
			
			$companyName = strip_tags($get_company_details['companyName']);
			$ownerName = strip_tags($get_company_details['ownerName']);
			$phoneNo=strip_tags($get_company_details['phoneNo']);
			$regNo=strip_tags($get_company_details['regNo']);
			$mobileNo = strip_tags($get_company_details['mobileNo']);
			
			$faxNo = strip_tags($get_company_details['faxNo']);
			$address = strip_tags($get_company_details['address']);
			$city=strip_tags($get_company_details['city']);
			$country = strip_tags($get_company_details['country']);
			$comEmail=strip_tags($get_company_details['comEmail']);
			$comWeb = strip_tags($get_company_details['comWeb']);
				
		//	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo
				
				
  				
?>
<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from thevectorlab.net/flatlab/dynamic_table.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 11 Dec 2013 05:50:27 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Online Sales And Inventory Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header white-bg">
            <?php include ("header.php");?>
        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <?php include("menu.php"); ?>
      </aside>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             Customer List
                          </header>
                          <div class="panel-body">
                                <div class="adv-table">
                                    <table  class="display table table-bordered table-striped" id="example">
                                      <thead>
                                      <tr>
                                          <th>Customer Name</th>
                                          <th>Company Name</th>
                                          <th>Phone No</th>
                                          <th>Mobile No</th>
                                          <th>Email</th>
                                          <th>Action</th>
                                      </tr>
                                      </thead>
                                      <?php


        // number of results to show per page

        $per_page = 50;

      // id 	title 	fname 	lname 	gender 	country 	district 	thana 	address 	mobile 	email 	username 	password 	introducer 	doj 	role 	dou 	status  

        // figure out the total pages in the database

        $result = mysql_query("SELECT * FROM customermaster ORDER BY customerId DESC");

        
		
		
		$total_results = mysql_num_rows($result);

        $total_pages = ceil($total_results / $per_page);



        // check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)

        if (isset($_GET['page']) && is_numeric($_GET['page']))

        {

                $show_page = $_GET['page'];

                

                // make sure the $show_page value is valid

                if ($show_page > 0 && $show_page <= $total_pages)

                {

                        $start = ($show_page -1) * $per_page;

                        $end = $start + $per_page; 

                }

                else

                {

                        // error - show first set of results

                        $start = 0;

                        $end = $per_page; 

                }               

        }

        else

        {

                // if page isn't set, show first set of results

                $start = 0;

                $end = $per_page; 

        }

        

       

        // display data in table

        

		echo "</tbody>";
		
		
// memCatId 	memCategory 	memCatValue 	comValue 	mutual_com
        // loop through results of database query, displaying them in the table 

        for ($i = $start; $i < $end; $i++)

        {

                // make sure that PHP doesn't try to show results that don't exist

                if ($i == $total_results) { break; }

        

                // echo out the contents of each row into a table

				//jobCatid 	jobCategory 	status
				
                echo "<tr>";
				
				//trackId,username,investAmount

                echo '<td>' . mysql_result($result, $i, 'customerName') . '</td>';
				
				echo '<td>' . mysql_result($result, $i, 'companyName') . '</td>';
				
				echo '<td>' . mysql_result($result, $i, 'phoneNo') . '</td>';
				
				echo '<td>' . mysql_result($result, $i, 'mobileNo') . '</td>';				
				echo '<td>' . mysql_result($result, $i, 'emailId') . '</td>';
				echo '<td>                                     
                   <span class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i><li><a  href="customerEdit.php?cid='.mysql_result($result, $i, 'customerId') . '">Edit</a></li></span>
                </td>';
				
				

        

                echo "</tr>"; 

        }

        // close table>

        echo "</tbody>"; 

         echo "</table>";

        // pagination

		

        

?>
                            
                            
        
                                     
                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2013 &copy; FlatLab by VectorLab.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <!--<script src="js/jquery.js"></script>-->
    <script type="text/javascript" language="javascript" src="assets/advanced-datatable/media/js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
    <script src="js/respond.min.js" ></script>


  <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>

    <!--script for this page only-->

      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#example').dataTable( {
                  "aaSorting": [[ 4, "desc" ]]
              } );
          } );
      </script>
  </body>

</html>
<?php
	}
}

}
else
{
	header("location: login.php");
	exit(); 
}
	
?>