-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2014 at 07:12 PM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sipm`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brandName` varchar(150) NOT NULL,
  `shortDescription` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `brand_t`
--

CREATE TABLE IF NOT EXISTS `brand_t` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brandName` varchar(100) NOT NULL,
  `subCatId` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `brand_t`
--

INSERT INTO `brand_t` (`id`, `brandName`, `subCatId`, `status`) VALUES
(18, 'Lephone', 10, 'Active'),
(17, '5Star', 10, 'Active'),
(16, 'Mycell', 10, 'Active'),
(19, 'Wootel', 10, 'Active'),
(20, 'Nokia', 10, 'Active'),
(21, 'ZXCV', 18, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `catgory`
--

CREATE TABLE IF NOT EXISTS `catgory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catName` varchar(100) NOT NULL,
  `catDescription` varchar(255) NOT NULL,
  `vat` float NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `catgory`
--

INSERT INTO `catgory` (`id`, `catName`, `catDescription`, `vat`, `status`) VALUES
(11, 'shampoo', '', 0, 'Active'),
(10, 'Mobile', '', 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(100) NOT NULL,
  `ownerName` varchar(100) NOT NULL,
  `regNo` varchar(25) NOT NULL,
  `phoneNo` varchar(100) NOT NULL,
  `mobileNo` varchar(100) NOT NULL,
  `faxNo` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `country` varchar(20) NOT NULL,
  `comEmail` varchar(100) NOT NULL,
  `comWeb` varchar(100) NOT NULL,
  `clogo` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `companyName`, `ownerName`, `regNo`, `phoneNo`, `mobileNo`, `faxNo`, `address`, `city`, `country`, `comEmail`, `comWeb`, `clogo`, `status`) VALUES
(2, 'Asian Global Ventures ', '', 'xkxkxk02993', '029850613', '01943217660', '029850614', 'House 3, Road 28, Block K, Banani, Dhaka, Bangladesh', 'Dhaka', 'Bangladesh', 'softwaresolutions@agvcorp.biz', 'www.agvcorp.biz', 'company/52b6a01e894fc.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `customerledger`
--

CREATE TABLE IF NOT EXISTS `customerledger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `transactionDate` date NOT NULL,
  `invoiceId` int(11) NOT NULL,
  `creditAmount` float NOT NULL,
  `debitAmount` float NOT NULL,
  `balanceAmount` float NOT NULL,
  `userId` int(11) NOT NULL,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customermaster`
--

CREATE TABLE IF NOT EXISTS `customermaster` (
  `customerId` int(11) NOT NULL AUTO_INCREMENT,
  `customerName` varchar(200) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `companyName` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `phoneNo` varchar(50) NOT NULL,
  `mobileNo` varchar(50) NOT NULL,
  `emailId` varchar(50) NOT NULL,
  `web` varchar(50) NOT NULL,
  `creditLimit` int(11) NOT NULL,
  `dtcreate` date NOT NULL,
  `dtModified` date NOT NULL,
  `createBy` varchar(10) NOT NULL,
  `status` varchar(200) NOT NULL,
  KEY `customerId` (`customerId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customermaster`
--

INSERT INTO `customermaster` (`customerId`, `customerName`, `gender`, `designation`, `companyName`, `address`, `phoneNo`, `mobileNo`, `emailId`, `web`, `creditLimit`, `dtcreate`, `dtModified`, `createBy`, `status`) VALUES
(1, 'Altaf Hossain', 'Male', 'Developer', 'BBS', 'Houes-09,Road-12,Block-K,Banani, Dhaka', '189451885', '1554841154', 'altaf@gmail.com', '', 0, '2013-12-28', '2013-12-28', 'admin', ''),
(2, 'Sultan', 'Male', 'Executive officer', 'AGV', 'Kafrul,Taltola', '12345789', '+8801716850984', 'sultan.agv@gmail.com', '', 0, '2013-12-29', '2013-12-29', 'admin', ''),
(3, 'Farhana', 'Female', 'Marketting execitive', 'AGV', 'Banani dhaka', '5462318789', '01716811011', 'farhan.agv@gmail.com', '', 0, '2013-12-29', '2013-12-29', 'admin', ''),
(4, 'Mr. Habib Sattar', 'Male', 'Managing Director', 'Consumark', 'dfkjsdf;adjf', '019090393', '02939303', 'habib@consumark.com', '', 0, '2014-03-19', '2014-03-19', 'demo', ''),
(5, 'Siddik Aslam', 'Male', 'Sales', 'TAra', 'dgsfdjfg', '74767', '365365', 'atsfd@jsdg.fkjd', '', 0, '2014-05-10', '2014-05-10', 'demo', '');

-- --------------------------------------------------------

--
-- Table structure for table `invoicedetails`
--

CREATE TABLE IF NOT EXISTS `invoicedetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceId` int(11) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `qty` int(5) NOT NULL,
  `unitPrice` float NOT NULL,
  `discount` float NOT NULL,
  `vat` float NOT NULL,
  `total` float NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=107 ;

--
-- Dumping data for table `invoicedetails`
--

INSERT INTO `invoicedetails` (`id`, `invoiceId`, `invoiceNo`, `productId`, `qty`, `unitPrice`, `discount`, `vat`, `total`, `status`) VALUES
(84, 0, 0, 29, 2, 25000, 0, 0, 50000, ''),
(83, 0, 0, 39, 5, 9000, 0, 0, 45000, ''),
(82, 0, 0, 40, 2, 600, 0, 150, 1350, ''),
(81, 0, 0, 30, 5, 1500, 0, 937.5, 8437.5, ''),
(80, 0, 0, 27, 2, 6580, 500, 443.1, 13103.1, ''),
(79, 0, 0, 41, 1, 6000, 250, 0, 5750, ''),
(78, 74, 47001, 28, 2, 10500, 0, 735, 21735, ''),
(77, 74, 47001, 29, 3, 25000, 25, 1499.5, 76474.5, ''),
(85, 0, 0, 29, 5, 25000, 0, 2500, 127500, ''),
(86, 0, 0, 32, 10, 70, 10, 24.15, 714.15, ''),
(87, 0, 0, 37, 10, 1000, 0, 0, 10000, ''),
(88, 0, 0, 38, 5, 1220, 0, 213.5, 6313.5, ''),
(89, 0, 0, 40, 2, 600, 200, 125, 1125, ''),
(90, 0, 0, 36, 10, 6000, 100, 0, 59900, ''),
(91, 0, 0, 34, 5, 3000, 0, 0, 15000, ''),
(92, 0, 0, 36, 10, 6000, 0, 0, 60000, ''),
(93, 0, 0, 29, 2, 25000, 1000, 0, 49000, ''),
(94, 0, 0, 40, 5, 600, 0, 0, 3000, ''),
(95, 0, 0, 37, 5, 1000, 100, 0, 4900, ''),
(96, 0, 0, 30, 10, 1500, 0, 0, 15000, ''),
(97, 76, 47002, 36, 2, 6000, 1000, 385, 11385, ''),
(98, 77, 47003, 31, 2, 50, 0, 12.5, 112.5, ''),
(99, 0, 0, 28, 1, 10500, 0, 0, 10500, ''),
(100, 0, 0, 27, 1, 6580, 300, 0, 6280, ''),
(101, 78, 47004, 42, 50, 875, 1000, 0, 42750, ''),
(102, 79, 47005, 44, 20, 250, 20, 174.3, 5154.3, ''),
(103, 80, 47006, 43, 5, 3000, 0, 0, 15000, ''),
(104, 0, 0, 42, 3, 875, 300, 0, 2325, ''),
(105, 81, 47007, 45, 11, 0, 0, 0, 0, ''),
(106, 0, 0, 43, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `invoicedraft`
--

CREATE TABLE IF NOT EXISTS `invoicedraft` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `draftDate` date NOT NULL,
  `productId` int(11) NOT NULL,
  `catId` int(11) NOT NULL,
  `qty` int(5) NOT NULL,
  `unitPrice` float NOT NULL,
  `discount` float NOT NULL,
  `vatChargeStatus` int(5) NOT NULL,
  `vatValue` float NOT NULL,
  `modifiedBy` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=472 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoicemaster`
--

CREATE TABLE IF NOT EXISTS `invoicemaster` (
  `invoiceId` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `invoiceType` varchar(20) NOT NULL,
  `invoiceTotal` float NOT NULL,
  `vatInclusive` float NOT NULL,
  `vatExclusive` float NOT NULL,
  `discountAmount` float NOT NULL,
  `othersCharges` float NOT NULL,
  `grandTotal` float NOT NULL,
  `paymentRcv` float NOT NULL,
  `creditAmount` float NOT NULL,
  `paymentStatus` varchar(10) NOT NULL,
  `returnId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `vStatus` int(2) NOT NULL,
  PRIMARY KEY (`invoiceId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `invoicemaster`
--

INSERT INTO `invoicemaster` (`invoiceId`, `invoiceNo`, `invoiceDate`, `customerId`, `invoiceType`, `invoiceTotal`, `vatInclusive`, `vatExclusive`, `discountAmount`, `othersCharges`, `grandTotal`, `paymentRcv`, `creditAmount`, `paymentStatus`, `returnId`, `userId`, `status`, `remarks`, `vStatus`) VALUES
(75, 547001, '2014-01-15', 1, '', 74975, 0, 1499.5, 25, 0, 76449.5, 0, 0, '', 0, 0, 'updt', '', 0),
(74, 47001, '2014-01-15', 1, '', 21000, 0, 735, 0, 0, 21735, 0, 0, '', 0, 0, 'updt', '', 1),
(73, 547000, '2014-01-15', 3, '', 627440, 0, 0, 0, 0, 627440, 0, 0, '', 0, 0, 'updt', '', 0),
(72, 47000, '2014-01-15', 3, '', 7000, 0, 875, 500, 0, 7375, 0, 0, '', 0, 0, 'updt', '', 1),
(76, 47002, '2014-01-20', 2, '', 11000, 0, 385, 1000, 0, 10385, 0, 0, '', 0, 0, 'updt', '', 1),
(77, 47003, '2014-01-20', 2, '', 100, 0, 12.5, 0, 0, 112.5, 0, 0, '', 0, 0, 'updt', '', 1),
(78, 47004, '2014-03-19', 4, '', 42750, 0, 0, 1000, 0, 41750, 0, 0, '', 0, 0, 'updt', '', 1),
(79, 47005, '2014-05-10', 5, '', 4980, 0, 174.3, 20, 0, 5134.3, 0, 0, '', 0, 0, 'updt', '', 1),
(80, 47006, '2014-05-10', 5, '', 15000, 0, 0, 0, 0, 15000, 0, 0, '', 0, 0, 'updt', '', 1),
(81, 47007, '2014-05-21', 1, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 'updt', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `locationId` int(11) NOT NULL AUTO_INCREMENT,
  `locationName` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`locationId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`locationId`, `locationName`, `status`) VALUES
(15, 'Bangkok', 'Active'),
(14, 'Dhaka', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `locationdetails`
--

CREATE TABLE IF NOT EXISTS `locationdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locationId` int(11) NOT NULL,
  `locationName` varchar(200) NOT NULL,
  `locationAddress` text NOT NULL,
  `locationPhone` varchar(15) NOT NULL,
  `locationMobile` varchar(15) NOT NULL,
  `locationFax` varchar(15) NOT NULL,
  `locationEmail` varchar(50) NOT NULL,
  `locationWeb` varchar(50) NOT NULL,
  `locationStatus` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `locationdetails`
--

INSERT INTO `locationdetails` (`id`, `locationId`, `locationName`, `locationAddress`, `locationPhone`, `locationMobile`, `locationFax`, `locationEmail`, `locationWeb`, `locationStatus`) VALUES
(3, 10, 'Mirpur', 'House-03,Road-28,Block K,Banani,Dhaka', '9850614', '01834185198', '9850613', 'info@agvcorp.biz', 'www.agvcorp.biz', ''),
(4, 9, 'Uttara', 'dgsgsgdgsd', '8504909', '01678504909', '01678504', 'taibur.agv@gmail.com', 'www.agvcorp.biz', ''),
(5, 12, 'Gulshan', '', '', '', '', '', '', ''),
(6, 12, 'Gulshan', '45', '1212', '123', '78', 'altaf.agv@gmail.com', '65', ''),
(7, 12, 'Gulshan', '', '', '', '', '', '', ''),
(8, 13, 'Mirpur', 'gsdsdg', '2345235', '235235', '23523', '235523@dfhf.gh', 'ffd.fh', ''),
(9, 16, 'Noakhali', '', '999999', '99999', '999999', '999999@gmail.com', 'wwww.aggg.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `paymentdetails`
--

CREATE TABLE IF NOT EXISTS `paymentdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchaseOrderId` int(11) NOT NULL,
  `invoiceId` int(11) NOT NULL,
  `paymentDate` date NOT NULL,
  `paymentType` varchar(10) NOT NULL,
  `paymentRcv` float NOT NULL,
  `checkNo` varchar(25) NOT NULL,
  `checkType` varchar(20) NOT NULL,
  `checkDate` date NOT NULL,
  `bankName` varchar(50) NOT NULL,
  `modifiedBy` varchar(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `paymentmaster`
--

CREATE TABLE IF NOT EXISTS `paymentmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceId` int(11) NOT NULL,
  `poId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `paymentType` varchar(10) NOT NULL,
  `cashAmount` float NOT NULL,
  `checkAmount` float NOT NULL,
  `checkNum` varchar(20) NOT NULL,
  `bankName` varchar(50) NOT NULL,
  `checkType` varchar(10) NOT NULL,
  `dateOfCheck` date NOT NULL,
  `checkStatus` varchar(10) NOT NULL,
  `paymentDate` date NOT NULL,
  `userId` varchar(50) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `paymentmaster`
--

INSERT INTO `paymentmaster` (`id`, `invoiceId`, `poId`, `supplierId`, `customerId`, `paymentType`, `cashAmount`, `checkAmount`, `checkNum`, `bankName`, `checkType`, `dateOfCheck`, `checkStatus`, `paymentDate`, `userId`, `status`) VALUES
(1, 0, 82, 5, 0, 'CASH', 270200, 0, '', '', '', '1970-01-01', '', '2014-01-19', '0', 'vendor payment'),
(2, 0, 88, 4, 0, 'CASH', 4000, 0, '', '', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(3, 0, 89, 4, 0, 'CHEQUE', 5400, 0, '123456789', 'DBBL', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(4, 0, 81, 4, 0, 'CHEQUE', 0, 6000, '654789321', 'EXIM', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(5, 0, 81, 4, 0, 'CASH', 6020, 0, '', '', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(6, 0, 88, 4, 0, 'CASH', 4350, 0, '', '', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(7, 0, 88, 4, 0, 'CHEQUE', 0, 4370, '89564', '', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(8, 0, 79, 3, 0, 'CASH', 15300, 0, '', '', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(9, 0, 90, 3, 0, 'CHEQUE', 0, 122300, '98745+6321', '', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(10, 0, 83, 5, 0, 'CASH', 200, 0, '', '', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(11, 0, 85, 2, 0, 'CASH', 5200, 0, '', '', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(12, 0, 79, 3, 0, 'CASH', 15300, 0, '', '', '', '1970-01-01', '', '2014-01-19', 'admin', 'vendor payment'),
(13, 0, 90, 3, 0, 'CASH', 5000, 0, '', '', '', '1970-01-01', '', '2014-01-21', 'demo', 'vendor payment'),
(14, 0, 90, 3, 0, 'CHEQUE', 0, 70000, '123456789', '', '', '1970-01-01', '', '2014-01-21', 'demo', 'vendor payment'),
(15, 0, 78, 2, 0, 'CASH', 180000, 0, '', '', '', '1970-01-01', '', '2014-01-22', 'demo', 'vendor payment'),
(16, 0, 83, 5, 0, 'CASH', 210, 0, '', '', '', '1970-01-01', '', '2014-03-19', 'demo', 'vendor payment'),
(17, 0, 94, 5, 0, 'CASH', 100, 0, '', '', '', '1970-01-01', '', '2014-03-19', 'demo', 'vendor payment'),
(18, 0, 93, 1, 0, 'CHEQUE', 0, 5000, '8789789', 'Prime Bank', '', '1970-01-01', '', '2014-03-19', 'demo', 'vendor payment'),
(19, 0, 78, 2, 0, 'CASH', 181100, 0, '', '', '', '1970-01-01', '', '2014-03-19', 'demo', 'vendor payment'),
(20, 0, 85, 2, 0, 'CASH', 5205, 0, '', '', '', '1970-01-01', '', '2014-03-19', 'demo', 'vendor payment'),
(21, 0, 80, 4, 0, 'CASH', 14700, 0, '', '', '', '1970-01-01', '', '2014-03-19', 'demo', 'vendor payment'),
(22, 0, 84, 4, 0, 'CASH', 59300, 0, '', '', '', '1970-01-01', '', '2014-03-19', 'demo', 'vendor payment'),
(23, 0, 84, 4, 0, 'CASH', 63105, 0, '', '', '', '1970-01-01', '', '2014-05-08', 'demo', 'vendor payment');

-- --------------------------------------------------------

--
-- Table structure for table `productpolicy_tbl`
--

CREATE TABLE IF NOT EXISTS `productpolicy_tbl` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `productId` int(10) NOT NULL,
  `policy_type` varchar(100) NOT NULL,
  `pwarranty_period` int(10) NOT NULL,
  `pguarante_period` int(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `productpolicy_tbl`
--

INSERT INTO `productpolicy_tbl` (`id`, `productId`, `policy_type`, `pwarranty_period`, `pguarante_period`, `status`) VALUES
(1, 20, 'CashBack', 10, 0, 'Active'),
(2, 20, 'Exchange', 21, 0, 'Active'),
(3, 20, 'Exchange', 21, 0, 'Active'),
(4, 21, 'CashBack', 54, 21, 'Active'),
(6, 23, 'CashBack', 30, 10, 'Active'),
(10, 28, 'Exchange', 365, 90, 'Active'),
(9, 27, 'Exchange', 356, 90, 'Active'),
(24, 43, 'Exchange', 1, 6, 'Active'),
(25, 44, 'CashBack', 365, 365, 'Active'),
(26, 45, 'CashBack', 0, 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `productprice`
--

CREATE TABLE IF NOT EXISTS `productprice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) NOT NULL,
  `unitPrice` float NOT NULL,
  `dtcreate` date NOT NULL,
  `dtmodified` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `userId` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `productprice`
--

INSERT INTO `productprice` (`id`, `productId`, `unitPrice`, `dtcreate`, `dtmodified`, `status`, `userId`) VALUES
(18, 13, 12000, '2013-12-30', '2013-12-30', 'Inactive', 'admin'),
(16, 12, 6000, '2013-12-30', '2013-12-30', 'Inactive', 'admin'),
(17, 12, 9000, '2013-12-30', '2013-12-30', 'Deleted', 'admin'),
(14, 11, 7500, '2013-12-30', '2013-12-30', 'Deleted', 'admin'),
(19, 13, 12500, '2013-12-30', '0000-00-00', 'Active', 'admin'),
(20, 14, 6580, '2013-12-30', '0000-00-00', 'Active', 'admin'),
(21, 15, 1500, '2013-12-30', '0000-00-00', 'Active', 'admin'),
(22, 16, 3000, '2013-12-30', '0000-00-00', 'Active', 'admin'),
(23, 17, 8500, '2013-12-31', '0000-00-00', 'Active', 'admin'),
(24, 18, 27000, '2013-12-31', '0000-00-00', 'Active', 'admin'),
(25, 19, 12, '2013-12-31', '2013-12-31', 'Deleted', 'admin'),
(26, 20, 1.56156e+06, '2013-12-31', '2013-12-31', 'Deleted', 'admin'),
(27, 21, 21, '2013-12-31', '2013-12-31', 'Deleted', 'admin'),
(28, 22, 12135, '2013-12-31', '2013-12-31', 'Deleted', 'admin'),
(29, 23, 12000, '2013-12-31', '0000-00-00', 'Active', 'admin'),
(30, 24, 15000, '2013-12-31', '0000-00-00', 'Active', 'admin'),
(31, 25, 15750, '2014-01-03', '2014-01-03', 'Deleted', 'admin'),
(32, 26, 15750, '2014-01-03', '2014-01-03', 'Deleted', 'admin'),
(33, 27, 6580, '2014-01-03', '0000-00-00', 'Active', 'admin'),
(34, 28, 10500, '2014-01-03', '0000-00-00', 'Active', 'admin'),
(35, 29, 25000, '2014-01-03', '2014-01-27', 'Deleted', 'admin'),
(36, 30, 1500, '2014-01-03', '2014-01-27', 'Deleted', 'admin'),
(37, 31, 50, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(38, 32, 70, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(39, 33, 7500, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(40, 34, 3000, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(41, 35, 5000, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(42, 36, 6000, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(43, 37, 1000, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(44, 38, 1220, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(45, 39, 9000, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(46, 40, 600, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(47, 41, 6000, '2014-01-07', '2014-01-27', 'Deleted', 'admin'),
(48, 42, 875, '2014-01-27', '0000-00-00', 'Active', 'demo'),
(49, 43, 3000, '2014-03-19', '0000-00-00', 'Active', 'demo'),
(50, 44, 250, '2014-05-10', '0000-00-00', 'Active', 'demo'),
(51, 45, 0, '2014-05-21', '0000-00-00', 'Active', 'demo');

-- --------------------------------------------------------

--
-- Table structure for table `product_t`
--

CREATE TABLE IF NOT EXISTS `product_t` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productName` varchar(100) NOT NULL,
  `catId` int(11) NOT NULL,
  `subCatId` int(11) NOT NULL,
  `brandId` int(11) NOT NULL,
  `proDescription` varchar(200) NOT NULL,
  `unitPrice` float NOT NULL,
  `reOrderLevel` int(5) NOT NULL,
  `minOrderQty` int(11) NOT NULL,
  `maxOrderQty` int(11) NOT NULL,
  `discountLevel` int(11) NOT NULL,
  `vatType` int(1) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `returnDays` int(5) NOT NULL,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `product_t`
--

INSERT INTO `product_t` (`id`, `productName`, `catId`, `subCatId`, `brandId`, `proDescription`, `unitPrice`, `reOrderLevel`, `minOrderQty`, `maxOrderQty`, `discountLevel`, `vatType`, `supplierId`, `returnDays`, `status`) VALUES
(43, 'C2-00', 10, 10, 20, '', 3000, 5, 100, 500, 10, 0, 0, 0, ''),
(42, 'K2+', 10, 0, 0, '', 875, 1, 1, 10, 0, 0, 0, 0, ''),
(27, 'Nokia X25', 7, 10, 5, '', 6580, 10, 10, 20, 0, 0, 0, 0, ''),
(28, 'Symphony 81', 7, 10, 7, '', 10500, 5, 5, 10, 0, 0, 0, 0, ''),
(44, 'Sample 1', 11, 18, 21, '', 250, 50, 500, 5000, 50, 0, 0, 0, ''),
(45, 'Johnson Baby Soap  75gm', 11, 18, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `purchasedetails`
--

CREATE TABLE IF NOT EXISTS `purchasedetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poId` int(11) NOT NULL,
  `pId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `qty` int(5) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `purchasedetails`
--

INSERT INTO `purchasedetails` (`id`, `poId`, `pId`, `purchaseAmount`, `qty`, `status`) VALUES
(19, 78, 32, 80, 10, 'Due'),
(18, 78, 35, 15000, 10, 'Due'),
(17, 77, 39, 9000, 16, 'Due'),
(16, 77, 37, 15000, 15, 'Due'),
(15, 77, 31, 40, 12, 'Due'),
(14, 77, 28, 7000, 10, 'Due'),
(13, 77, 27, 5000, 10, 'Due'),
(12, 77, 29, 400, 5, 'Due'),
(11, 77, 30, 200, 10, 'Due'),
(20, 78, 36, 2000, 15, 'Due'),
(21, 79, 40, 200, 15, 'Due'),
(22, 79, 32, 200, 10, 'Due'),
(23, 79, 38, 1000, 10, 'Due'),
(24, 80, 31, 60, 10, 'Due'),
(25, 80, 29, 500, 10, 'Due'),
(26, 80, 30, 900, 10, 'Due'),
(27, 81, 29, 600, 10, 'Due'),
(28, 82, 41, 8000, 30, 'Due'),
(29, 82, 35, 3000, 10, 'Due'),
(30, 83, 27, 20, 0, 'Due'),
(31, 84, 27, 2580, 10, 'Due'),
(32, 84, 36, 2466, 15, 'Due'),
(38, 91, 30, 5000, 10, 'Due'),
(34, 89, 31, 25, 15, 'Due'),
(35, 89, 37, 250, 20, 'Due'),
(36, 90, 27, 450, 10, 'Due'),
(37, 90, 35, 7850, 15, 'Due'),
(39, 93, 28, 1000, 10, 'Due'),
(40, 94, 31, 14, 12, 'Due'),
(41, 94, 33, 12, 12, 'Due'),
(42, 95, 43, 3000, 150, 'Due'),
(43, 96, 27, 12000, 10, 'Due'),
(44, 97, 42, 5890, 2, 'Due'),
(45, 97, 28, 45678, 5, 'Due');

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorder`
--

CREATE TABLE IF NOT EXISTS `purchaseorder` (
  `poId` int(11) NOT NULL AUTO_INCREMENT,
  `supplierId` int(11) NOT NULL,
  `transactionDate` date NOT NULL,
  `transactionTotal` float NOT NULL,
  `transactionVat` float NOT NULL,
  `othersAmount` float NOT NULL,
  `transactionGtotal` float NOT NULL,
  `paidAmount` float NOT NULL,
  `dueAmount` float NOT NULL,
  `returnId` int(11) NOT NULL,
  `modifiedBy` varchar(10) NOT NULL,
  `status` varchar(5) NOT NULL,
  PRIMARY KEY (`poId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Dumping data for table `purchaseorder`
--

INSERT INTO `purchaseorder` (`poId`, `supplierId`, `transactionDate`, `transactionTotal`, `transactionVat`, `othersAmount`, `transactionGtotal`, `paidAmount`, `dueAmount`, `returnId`, `modifiedBy`, `status`) VALUES
(79, 3, '2014-01-07', 15000, 300, 0, 15300, 15300, 0, 0, 'admin', 'Paid'),
(78, 2, '2014-01-07', 180800, 300, 0, 181100, 181100, 0, 0, 'admin', 'Paid'),
(77, 1, '2014-01-07', 493480, 200, 0, 493680, 493680, 0, 0, 'admin', 'Paid'),
(80, 4, '2014-01-07', 14600, 100, 0, 14700, 14700, 0, 0, 'admin', 'Paid'),
(81, 4, '2014-01-07', 6000, 20, 0, 6020, 6020, 0, 0, 'admin', 'Paid'),
(82, 5, '2014-01-07', 270000, 200, 0, 270200, 270200, 0, 0, 'admin', 'Paid'),
(83, 5, '2014-01-07', 200, 10, 0, 210, 210, 0, 0, 'admin', 'Paid'),
(84, 4, '2014-01-08', 63105, 0, 0, 63105, 63105, 0, 0, 'admin', 'Paid'),
(85, 2, '2014-01-08', 5205, 0, 0, 5205, 5205, 0, 0, 'admin', 'Paid'),
(86, 1, '2014-01-08', 875, 0, 0, 875, 875, 0, 0, 'admin', 'Paid'),
(87, 1, '2014-01-08', 875, 0, 0, 875, 875, 0, 0, 'admin', 'Paid'),
(88, 4, '2014-01-08', 4370, 0, 0, 4370, 4370, 0, 0, 'admin', 'Paid'),
(89, 4, '2014-01-08', 5375, 25, 0, 5400, 5400, 0, 0, 'admin', 'Paid'),
(90, 3, '2014-01-08', 122250, 50, 0, 122300, 70000, 52300, 0, 'admin', 'Due'),
(91, 4, '2014-01-19', 50000, 10, 0, 50010, 0, 50010, 0, 'admin', 'Due'),
(92, 4, '2014-01-20', 0, 0, 0, 0, 0, 0, 0, 'demo', 'Due'),
(93, 1, '2014-01-20', 10000, 100, 0, 10100, 5000, 5100, 0, 'demo', 'Due'),
(94, 5, '2014-01-20', 312, 0, 0, 312, 100, 212, 0, 'demo', 'Due'),
(96, 3, '2014-05-08', 120000, 1500, 0, 121500, 0, 121500, 0, 'demo', 'Due'),
(95, 1, '2014-03-19', 450000, 0, 0, 0, 0, 0, 0, 'demo', 'Due'),
(97, 5, '2014-05-10', 240170, 345, 0, 240515, 0, 240515, 0, 'demo', 'Due');

-- --------------------------------------------------------

--
-- Table structure for table `returnproduct`
--

CREATE TABLE IF NOT EXISTS `returnproduct` (
  `returnId` int(11) NOT NULL AUTO_INCREMENT,
  `returnDate` date NOT NULL,
  `purchaseOrderId` int(11) NOT NULL,
  `invoiceId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `qty` int(5) NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `status` varchar(50) NOT NULL,
  `modifiedBy` varchar(10) NOT NULL,
  PRIMARY KEY (`returnId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `returnproduct`
--

INSERT INTO `returnproduct` (`returnId`, `returnDate`, `purchaseOrderId`, `invoiceId`, `productId`, `qty`, `remarks`, `status`, `modifiedBy`) VALUES
(1, '2014-01-19', 77, 0, 39, 110, 'Damaged', 'received', 'admin'),
(2, '2014-01-20', 83, 0, 27, 10, 'XZXZXZXZX', 'received', 'demo'),
(3, '2014-05-10', 93, 0, 28, 1, 'Expired/ Fault', 'pending', 'demo');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `proVatType` int(5) NOT NULL,
  `vatAmount` float NOT NULL,
  `invoiceCategory` int(5) NOT NULL,
  `invoiceNoFormat` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`proVatType`, `vatAmount`, `invoiceCategory`, `invoiceNoFormat`) VALUES
(0, 500, 0, '24-05-2014');

-- --------------------------------------------------------

--
-- Table structure for table `stockdetails`
--

CREATE TABLE IF NOT EXISTS `stockdetails` (
  `stockId` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) NOT NULL,
  `transactionDate` date NOT NULL,
  `transactionType` varchar(50) NOT NULL,
  `purchaseOrderId` int(11) NOT NULL,
  `invoiceId` int(11) NOT NULL,
  `returnId` int(11) NOT NULL,
  `receiveQty` int(11) NOT NULL,
  `deliveryQty` int(11) NOT NULL,
  `modifiedBy` varchar(10) NOT NULL,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`stockId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `stockdetails`
--

INSERT INTO `stockdetails` (`stockId`, `productId`, `transactionDate`, `transactionType`, `purchaseOrderId`, `invoiceId`, `returnId`, `receiveQty`, `deliveryQty`, `modifiedBy`, `status`) VALUES
(58, 27, '2014-01-07', 'Purchased', 83, 0, 0, 10, 0, 'admin', 'pin'),
(57, 35, '2014-01-07', 'Purchased', 82, 0, 0, 10, 0, 'admin', 'pin'),
(56, 41, '2014-01-07', 'Purchased', 82, 0, 0, 30, 0, 'admin', 'pin'),
(55, 41, '2014-01-07', 'OpeningBalance', 0, 0, 0, 0, 0, 'admin', ''),
(54, 29, '2014-01-07', 'Purchased', 81, 0, 0, 10, 0, 'admin', 'pin'),
(53, 30, '2014-01-07', 'Purchased', 80, 0, 0, 10, 0, 'admin', 'pin'),
(52, 29, '2014-01-07', 'Purchased', 80, 0, 0, 10, 0, 'admin', 'pin'),
(51, 31, '2014-01-07', 'Purchased', 80, 0, 0, 10, 0, 'admin', 'pin'),
(50, 38, '2014-01-07', 'Purchased', 79, 0, 0, 10, 0, 'admin', 'pin'),
(49, 32, '2014-01-07', 'Purchased', 79, 0, 0, 10, 0, 'admin', 'pin'),
(48, 40, '2014-01-07', 'Purchased', 79, 0, 0, 15, 0, 'admin', 'pin'),
(47, 36, '2014-01-07', 'Purchased', 78, 0, 0, 15, 0, 'admin', 'pin'),
(46, 32, '2014-01-07', 'Purchased', 78, 0, 0, 10, 0, 'admin', 'pin'),
(45, 35, '2014-01-07', 'Purchased', 78, 0, 0, 10, 0, 'admin', 'pin'),
(44, 39, '2014-01-07', 'Purchased', 77, 0, 0, 16, 0, 'admin', 'pin'),
(43, 37, '2014-01-07', 'Purchased', 77, 0, 0, 15, 0, 'admin', 'pin'),
(42, 31, '2014-01-07', 'Purchased', 77, 0, 0, 12, 0, 'admin', 'pin'),
(41, 28, '2014-01-07', 'Purchased', 77, 0, 0, 10, 0, 'admin', 'pin'),
(40, 27, '2014-01-07', 'Purchased', 77, 0, 0, 10, 0, 'admin', 'pin'),
(39, 29, '2014-01-07', 'Purchased', 77, 0, 0, 5, 0, 'admin', 'pin'),
(38, 30, '2014-01-07', 'Purchased', 77, 0, 0, 10, 0, 'admin', 'pin'),
(59, 27, '2014-01-08', 'Purchased', 84, 0, 0, 10, 0, 'admin', 'pin'),
(60, 36, '2014-01-08', 'Purchased', 84, 0, 0, 15, 0, 'admin', 'pin'),
(61, 0, '2014-01-08', 'Purchased', 84, 0, 0, 0, 0, 'admin', 'pin'),
(62, 31, '2014-01-08', 'Purchased', 89, 0, 0, 15, 0, 'admin', 'pin'),
(63, 37, '2014-01-08', 'Purchased', 89, 0, 0, 20, 0, 'admin', 'pin'),
(64, 27, '2014-01-08', 'Purchased', 90, 0, 0, 10, 0, 'admin', 'pin'),
(65, 35, '2014-01-08', 'Purchased', 90, 0, 0, 15, 0, 'admin', 'pin'),
(66, 39, '2014-01-19', 'return', 0, 0, 1, 0, 110, 'admin', 'pout'),
(67, 30, '2014-01-19', 'Purchased', 91, 0, 0, 10, 0, 'admin', 'pin'),
(68, 28, '2014-01-20', 'Purchased', 93, 0, 0, 10, 0, 'demo', 'pin'),
(69, 27, '2014-01-20', 'return', 0, 0, 2, 0, 10, 'demo', 'pout'),
(70, 31, '2014-01-20', 'Purchased', 94, 0, 0, 12, 0, 'demo', 'pin'),
(71, 33, '2014-01-20', 'Purchased', 94, 0, 0, 12, 0, 'demo', 'pin'),
(72, 42, '2014-01-27', 'OpeningBalance', 0, 0, 0, 0, 0, 'demo', ''),
(73, 43, '2014-03-19', 'OpeningBalance', 0, 0, 0, 0, 0, 'demo', ''),
(74, 43, '2014-03-19', 'Purchased', 95, 0, 0, 150, 0, 'demo', 'pin'),
(75, 27, '2014-05-08', 'Purchased', 96, 0, 0, 10, 0, 'demo', 'pin'),
(76, 44, '2014-05-10', 'OpeningBalance', 0, 0, 0, 0, 0, 'demo', ''),
(77, 42, '2014-05-10', 'Purchased', 97, 0, 0, 2, 0, 'demo', 'pin'),
(78, 28, '2014-05-10', 'Purchased', 97, 0, 0, 5, 0, 'demo', 'pin'),
(79, 28, '2014-05-10', 'return', 0, 0, 3, 0, 1, 'demo', 'pout'),
(80, 45, '2014-05-21', 'OpeningBalance', 0, 0, 0, 0, 0, 'demo', '');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subCatName` varchar(150) NOT NULL,
  `subCatDesciption` varchar(255) NOT NULL,
  `catId` int(11) NOT NULL,
  `status` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `subCatName`, `subCatDesciption`, `catId`, `status`) VALUES
(10, 'Mobile SUB Categ', '', 10, 'Active'),
(18, 'GHJK', '', 11, 'Active'),
(17, 'Other', '', 9, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `supplierledger`
--

CREATE TABLE IF NOT EXISTS `supplierledger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplierId` int(11) NOT NULL,
  `transactionDate` date NOT NULL,
  `purchaseOrderId` int(11) NOT NULL,
  `purchaseTotal` float NOT NULL,
  `creditAmount` float NOT NULL,
  `debitAmount` float NOT NULL,
  `balanceAmount` float NOT NULL,
  `returnId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `supplierledger`
--

INSERT INTO `supplierledger` (`id`, `supplierId`, `transactionDate`, `purchaseOrderId`, `purchaseTotal`, `creditAmount`, `debitAmount`, `balanceAmount`, `returnId`, `userId`, `status`) VALUES
(8, 4, '2014-01-07', 80, 0, 14700, 0, 0, 0, 0, 'Due'),
(7, 3, '2014-01-07', 79, 0, 15300, 0, 0, 0, 0, 'Due'),
(6, 2, '2014-01-07', 78, 0, 181100, 0, 0, 0, 0, 'Due'),
(5, 1, '2014-01-07', 77, 0, 493680, 0, 0, 1, 0, 'Due'),
(9, 4, '2014-01-07', 81, 6020, 6020, 0, 0, 0, 0, 'Due'),
(10, 5, '2014-01-07', 82, 270200, 270200, 0, 0, 0, 0, 'Due'),
(11, 5, '2014-01-07', 83, 210, 210, 0, 0, 2, 0, 'Due'),
(12, 4, '2014-01-08', 84, 0, 63105, 0, 0, 0, 0, 'Due'),
(13, 4, '2014-01-08', 89, 0, 5400, 0, 0, 0, 0, 'Due'),
(14, 3, '2014-01-08', 90, 122300, 122300, 0, 0, 0, 0, 'Due'),
(15, 4, '2014-01-19', 91, 50010, 50010, 0, 0, 0, 0, 'Due'),
(16, 1, '2014-01-20', 93, 10100, 10100, 0, 0, 3, 0, 'Due'),
(17, 5, '2014-01-20', 94, 312, 312, 0, 0, 0, 0, 'Due'),
(18, 1, '2014-03-19', 95, 0, 0, 0, 0, 0, 0, 'Due'),
(19, 3, '2014-05-08', 96, 121500, 121500, 0, 0, 0, 0, 'Due'),
(20, 5, '2014-05-10', 97, 240515, 240515, 0, 0, 0, 0, 'Due');

-- --------------------------------------------------------

--
-- Table structure for table `suppliermaster`
--

CREATE TABLE IF NOT EXISTS `suppliermaster` (
  `supplierId` int(11) NOT NULL AUTO_INCREMENT,
  `supplierName` varchar(200) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `companyName` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `phoneNo` varchar(50) NOT NULL,
  `mobileNo` varchar(50) NOT NULL,
  `emailId` varchar(50) NOT NULL,
  `web` varchar(50) NOT NULL,
  `creditLimit` int(11) NOT NULL,
  `dtcreate` date NOT NULL,
  `dtModified` date NOT NULL,
  `createBy` varchar(10) NOT NULL,
  `status` varchar(200) NOT NULL,
  KEY `supplierId` (`supplierId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `suppliermaster`
--

INSERT INTO `suppliermaster` (`supplierId`, `supplierName`, `gender`, `designation`, `companyName`, `address`, `phoneNo`, `mobileNo`, `emailId`, `web`, `creditLimit`, `dtcreate`, `dtModified`, `createBy`, `status`) VALUES
(1, 'Elias', 'Male', 'admin', 'Leo King', 'mohakhali, Dhaka', '99885542', '01916811011', 'elias@leoking.com', '', 0, '2013-12-28', '2013-12-28', 'admin', ''),
(2, 'Muhsinul Islam', 'Male', 'Accountant', 'AGV', 'Dhaka,Bangladesh', '6598745621', '698745621', 'muhsinul.agv@gmail.com', '', 0, '2013-12-29', '2013-12-29', 'admin', ''),
(3, 'Farhana', 'Male', 'Marketting execitive', 'Farnhana & surjo Co. Ltd', 'Cantonment', '017382545412', '017382545412', 'farhan.agv@gmail.com', '', 0, '2014-01-07', '2014-01-07', 'admin', ''),
(4, 'Enamul Hoque', 'Male', 'Manager', 'Enamul & his brothers Ltd', 'Rajshahi', '017854215', '32145687', 'enamul.en@gmail.com', '', 0, '2014-01-07', '2014-01-07', 'admin', ''),
(5, 'Muhsenul Haque', 'Male', 'Manager', 'Jara House', 'Nban', '6598745', '0175846897', 'muhsenul.agv@gmail.com', '', 0, '2014-01-07', '2014-01-07', 'admin', ''),
(6, 'sxy', 'Male', 'gj', 'mnb', 'jojpp', '0133', '641..1.455', 'kjhh', '', 0, '2014-04-10', '2014-04-10', 'demo', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userFname` varchar(50) NOT NULL,
  `userLname` varchar(50) NOT NULL,
  `userId` varchar(10) NOT NULL,
  `password` varchar(15) NOT NULL,
  `mobileNo` varchar(15) NOT NULL,
  `emailId` varchar(50) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(25) NOT NULL,
  `country` varchar(15) NOT NULL,
  `refName` varchar(50) NOT NULL,
  `refMobile` varchar(15) NOT NULL,
  `joinDate` date NOT NULL,
  `designation` varchar(15) NOT NULL,
  `barnchId` varchar(15) NOT NULL,
  `companyId` varchar(15) NOT NULL,
  `nationalId` varchar(20) NOT NULL,
  `role` int(5) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `userFname`, `userLname`, `userId`, `password`, `mobileNo`, `emailId`, `gender`, `address`, `city`, `country`, `refName`, `refMobile`, `joinDate`, `designation`, `barnchId`, `companyId`, `nationalId`, `role`, `status`) VALUES
(1, 'Taibur', 'Rahman', 'admin', '123456789', '01834185198', 'taibur.agv@gmail.com', 'Male', 'H-145/CA,mirpur 14', 'Dhaka', 'Bangladesh', 'Altaf Hossain', '01678504909', '2013-12-01', 'admin', 'Dhaka', 'agv', 'ghsdyu17', 3, ''),
(3, 'Sultan', 'Ahmed', 'sultan', 'sultan12345', '01678504910', 'sultan@agvcorp.biz', 'Male', 'House-03,Road-28,Block-K,Banani,Dhaka', 'Dhaka', 'BD', 'Taibur Rahman', '01678504909', '2013-12-26', 'sales', '7', 'Asian Global Ve', '123456789789', 5, ''),
(5, 'Muhsinul', 'Islam', 'Muhsenul', '123456789', '01916811011', 'muhsenul.agv@gmail.com', 'Male', 'Dhaka,Bangladesh', 'Rajshahi', 'BD', 'Taibur', '016584', '2012-12-12', 'Accountant', '7', 'AGVBD', '54879546', 5, ''),
(6, 'Firoj', 'Ahmed', 'firoj', '123456', '01738244446', 'firoj.agv@gmail.com', 'Male', 'Gulshan,Dhaka', '', '', '', '', '2014-01-19', '', '12', '', '', 5, ''),
(7, 'demostration', '', 'demo', 'demo', '', '', '', '', '', '', '', '', '2013-05-20', '', '', '', '', 3, ''),
(8, 'Taibur', 'Rahman', 'taibur', '123456', '01678504909', 'trshurjo@gmail.com', 'Male', 'kjdsfksdj', '', '', 'Ashfaqur Rahman', '01834185198', '0000-00-00', 'Sales', '14', '', '', 5, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
